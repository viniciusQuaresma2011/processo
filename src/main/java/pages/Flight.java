package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Flight {
	
	protected WebDriver driver;
	
	private By flightFrom = By.xpath("//*[@id=\"data\"]/ul/li[1]/div/form/div/div[1]/div[1]/div[2]/div/div/div/div/div[2]/div/div/div[1]/div/p[2]");
	private By flightDestination = By.xpath("//*[@id=\"data\"]/ul/li[1]/div/form/div/div[1]/div[1]/div[2]/div/div/div/div/div[2]/div/div/div[3]/div/p[2]");
	
	
	public Flight(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getFlightFrom() {
		return driver.findElement(flightFrom).getText();
	}
	
	public String getFlightDestination() {
		return driver.findElement(flightDestination).getText();
	}
}
