package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class Search {
	protected WebDriver driver;
	
	private By flights = By.xpath("//*[@id=\"flights-tab\"]/span[2]/span");
	private By oneWay = By.xpath("//*[@id=\"one-way\"]");
	private By roundTrip = By.xpath("//*[@id=\"round-trip\"]");
	private By typeFlight = By.xpath("//*[@id=\"flight_type\"]");
	private By flyingFrom = By.xpath("//*[@id=\"autocomplete\"]");
	private By toDestination = By.xpath("//*[@id=\"autocomplete2\"]");
	private By departureDate = By.xpath("//*[@id=\"departure\"]");
	private By returnDate = By.xpath("//*[@id=\"return\"]");
	private By passengers = By.xpath("//*[@id=\"onereturn\"]/div[3]/div/div/div/a");
	
	private By passengersAdultOptionButton = By.xpath("//*[@id=\"onereturn\"]/div[3]/div/div/div/div/div[1]/div/div/div[2]/i");
	private By passengersChildsOptionButton = By.xpath("//*[@id=\"onereturn\"]/div[3]/div/div/div/div/div[2]/div/div/div[2]/i");
	private By passengersInfantsOptionButton = By.xpath("//*[@id=\"onereturn\"]/div[3]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]");
	
	private By passengersAdultOptionInput = By.xpath("//*[@id=\"fadults\"]");
	private By passengersChildsOptionInput = By.cssSelector("input#fchilds");
	private By passengersInfantsOptionInput = By.className("qtyInput_flights");
	
	
	private By searchButton = By.xpath("//*[@id=\"flights-search\"]");
	
	
	
	
	public Search(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickFlights() {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(flights).click();
	}
	
	public void clickOneWay() {
		driver.findElement(oneWay).click();
	}
	
	public void clickRoundTrip() {
		driver.findElement(roundTrip).click();
	}
	
	public Select findTypeFlightOptions() {
		return new Select(driver.findElement(typeFlight));
	}
	
	public void clickTypeFlight() {
		driver.findElement(typeFlight).click();
	}
	
	public void selectOptionInTypeFlight(String option) {
		findTypeFlightOptions().selectByVisibleText(option);
	}
	
	public void clickFlyingFrom() {
		driver.findElement(flyingFrom).click();
	}
	
	public void inputFlyingFrom(String text) {
		clickFlyingFrom();
		driver.findElement(flyingFrom).sendKeys(text);
	}
	
	public void clickToDestination() {
		driver.findElement(toDestination).click();
	}
	public void inputToDestination(String text) throws InterruptedException {
		clickToDestination();
		driver.findElement(toDestination).sendKeys(text);
		Thread.sleep(2000);
	}
	
	public void clickDepartureDate() {
		driver.findElement(departureDate).click();
		driver.findElement(departureDate).clear();
	}
	
	public void inputDepartureDate(String text) {
		clickDepartureDate();
		driver.findElement(departureDate).sendKeys(text);
	}
	
	
	public void clickReturnDate() {
		driver.findElement(returnDate).click();
		driver.findElement(returnDate).clear();
	}
	
	public void inputReturnDate(String text) {
		clickReturnDate();
		driver.findElement(returnDate).sendKeys(text);
	}
	
	public void clickPassengersAdult() {
		clickPassengers();
		driver.findElement(passengersAdultOptionButton).click();
	}
	
	public void clickPassengersChilds() {
		driver.findElement(passengersChildsOptionButton).click();
	}
	
	public void clickPassengersInfants() {
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(passengersInfantsOptionButton);
		action.moveToElement(we).click().build().perform();
	}
	
	
	
	public void inputPassengersAdult(String string) {
		clickPassengers();
		driver.findElement(passengersAdultOptionInput).click();
		driver.findElement(passengersAdultOptionInput).clear();
		driver.findElement(passengersAdultOptionInput).sendKeys(string);
	}
	
	
	public void inputPassengersChilds(String string) {
		driver.findElement(passengersChildsOptionInput).click();
		driver.findElement(passengersChildsOptionInput).clear();
		driver.findElement(passengersChildsOptionInput).sendKeys(string);
	}

	
	public void inputPassengersInfants(String string) {
		driver.findElement(passengersInfantsOptionInput).click();
		driver.findElement(passengersInfantsOptionInput).clear();
		driver.findElement(passengersInfantsOptionInput).sendKeys(string);
	}
	
	public void clickPassengers() {
		driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		driver.findElement(passengers).click();
	}
	
	public Select findPassengersOptions() {
		return new Select(driver.findElement(passengers));
	}
	
	public void clickSearchButton() {
		driver.findElement(searchButton).click();
	}

}
