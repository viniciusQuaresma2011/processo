package steps;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.Flight;
import pages.Search;

public class SearchTestSteps {
	public static WebDriver driver;
	
	
	public Search search;
	
	public static void init() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\webdriver\\chromedriver\\108\\chromedriver.exe");
		driver = new ChromeDriver();
		Thread.sleep(4000);
		driver.manage().window().maximize();
		driver.get("https://phptravels.net/");
	}
	


	@Given("that I am on the flight search screen")
	public void that_i_am_on_the_flight_search_screen() throws InterruptedException {
		init();
		
		
		search = new Search(driver);
		search.clickFlights();
	    
	}

	@Given("check the one-way option")
	public void check_the_one_way_option() {
		search = new Search(driver);
		search.clickOneWay();
	    
	}
	
	@Given("check the round-trip option")
	public void check_the_round_trip_option() {
		search = new Search(driver);
		search.clickRoundTrip();
	}

	@Given("select the {string} type flight")
	public void select_the_economy_type_flight(String string) {
		search = new Search(driver);
		search.selectOptionInTypeFlight(string);
	  
	}

	@Given("fill in the field flying from with  {string}")
	public void fill_in_the_field_flying_from_with_the_data(String string) {
		search = new Search(driver);
		search.inputFlyingFrom(string);
	   
	}

	@Given("fill in the field with destination with  {string}")
	public void fill_in_the_field_with_destination_with_the_data(String string) throws InterruptedException {
		search = new Search(driver);
		search.inputToDestination(string);
	  
	}

	@Given("fill in the departure date with the data: {string}")
	public void fill_in_the_departure_date_with_the_data(String string) {
		search = new Search(driver);
		search.inputDepartureDate(string);
	    
	}

	@Given("fill in the {string} passengers Adults")
	public void fill_in_the_passengers_adults(String string) {
		search = new Search(driver);
		search.inputPassengersAdult(string);
	  
	}

	@Given("fill in the {string} passengers Childs")
	public void fill_in_the_passengers_childs(String string) {
		search = new Search(driver);
		search.inputPassengersChilds(string);
	    
	}

	@Given("fill in the {string} passengers Infants")
	public void fill_in_the_passengers_infants(String string) {
		search = new Search(driver);
		search.inputPassengersInfants(string);
	   
	}

	@When("click on the search button")
	public void click_on_the_search_button() throws InterruptedException {
		search = new Search(driver);
		search.clickSearchButton();
		Thread.sleep(6000);
	    
	}

	@Then("view the listing with the flights and their respective prices\\/companies")
	public void view_the_listing_with_the_flights_and_their_respective_prices_companies() {
		
		Flight flight = new Flight(driver);
		
		String flyingFromSigle = "DXB";
		String flyingFromSingleElement = flight.getFlightFrom();
		assertEquals(flyingFromSigle, flyingFromSingleElement);
		
		String flyingDestination = "RML";
		String flyingDestinationElement = flight.getFlightDestination();
		assertEquals(flyingDestination, flyingDestinationElement);
		//printScreen("search", "valid");
		
	}

	@When("verify the pop-up with message error {string}")
	public void verify_the_pop_up_with_message_error(String string) {
		Alert alert = driver.switchTo().alert();	
		
		 String alertMessage= driver.switchTo().alert().getText();	
		 String popUpWithError = string;
		 
		 assertEquals(popUpWithError , alertMessage);
	}
	
	
	@After
	public void close() {
		driver.quit();
	}


}
