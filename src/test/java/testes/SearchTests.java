package testes;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;


import base.BaseTest;
import pages.Flight;
import pages.Search;

public class SearchTests extends BaseTest {
	
	private Search search = new Search(driver);
	private Flight flight = new Flight(driver);
	

	@Test
	public void searchOneFlyValid() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Economy";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "RML";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-01-2023";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		String flyingFromSigle = "DXB";
		String flyingFromSingleElement = flight.getFlightFrom();
		assertEquals(flyingFromSigle, flyingFromSingleElement);
		
		String flyingDestination = "RML";
		String flyingDestinationElement = flight.getFlightDestination();
		assertEquals(flyingDestination, flyingDestinationElement);
		printScreen("search", "valid");
	}
	
	@Test
	public void searchOneFlyValidRoundTrip() throws InterruptedException {
		
		search.clickFlights();
		search.clickRoundTrip();
		
		String optionInTypeFlight = "Economy";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "RML";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-01-2023";
		search.inputDepartureDate(dateTravel);
		
		String dateTravelReturn = "11-02-2023";
		search.inputReturnDate(dateTravelReturn);
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		String flyingFromSigle = "DXB";
		String flyingFromSingleElement = flight.getFlightFrom();
		assertEquals(flyingFromSigle, flyingFromSingleElement);
		
		String flyingDestination = "RML";
		String flyingDestinationElement = flight.getFlightDestination();
		assertEquals(flyingDestination, flyingDestinationElement);
		printScreen("roundTrip", "valid");
	}
	
	
	@Test
	public void searchOneFlyWithModeEconomy() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Economy";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "RML";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-01-2023";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		String flyingFromSigle = "DXB";
		String flyingFromSingleElement = flight.getFlightFrom();
		assertEquals(flyingFromSigle, flyingFromSingleElement);
		
		String flyingDestination = "RML";
		String flyingDestinationElement = flight.getFlightDestination();
		assertEquals(flyingDestination, flyingDestinationElement);
		printScreen("mode_economy", "valid");
	}
	
	@Test
	public void searchOneFlyWithModeEconomyPremium() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Economy Premium";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "RML";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-01-2023";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		String flyingFromSigle = "DXB";
		String flyingFromSingleElement = flight.getFlightFrom();
		assertEquals(flyingFromSigle, flyingFromSingleElement);
		
		String flyingDestination = "RML";
		String flyingDestinationElement = flight.getFlightDestination();
		assertEquals(flyingDestination, flyingDestinationElement);
		printScreen("mode_economy_premium", "valid");
	}
	
	@Test
	public void searchOneFlyWithModeBusiness() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Business";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "RML";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-01-2023";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		String flyingFromSigle = "DXB";
		String flyingFromSingleElement = flight.getFlightFrom();
		assertEquals(flyingFromSigle, flyingFromSingleElement);
		
		String flyingDestination = "RML";
		String flyingDestinationElement = flight.getFlightDestination();
		assertEquals(flyingDestination, flyingDestinationElement);
		printScreen("mode_business", "valid");
	}
	
	@Test
	public void searchOneFlyWithModeFirst() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "First";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "RML";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-01-2023";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		String flyingFromSigle = "DXB";
		String flyingFromSingleElement = flight.getFlightFrom();
		assertEquals(flyingFromSigle, flyingFromSingleElement);
		
		String flyingDestination = "RML";
		String flyingDestinationElement = flight.getFlightDestination();
		assertEquals(flyingDestination, flyingDestinationElement);
		printScreen("mode_first", "valid");
	}
	
	@Test
	public void searchOneFlyInvalidWithInputFlightFromInvalid() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Economy";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "Q!!@#!@$W%@&¨%$";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "CMB";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-01-2023";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		printScreen("flyingFrom", "invalid");
		
		Alert alert = driver.switchTo().alert();	
		
		 String alertMessage= driver.switchTo().alert().getText();	
		 
		 
		 String popUpWithError = "Please fill out origin";
		 
		 assertEquals(popUpWithError , alertMessage);
		 
	}
	
	
	@Test
	public void searchOneFlyInvalidWithInputFlightFromNull() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Economy";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "CMB";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-01-2023";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		Alert alert = driver.switchTo().alert();	
		
		 String alertMessage= driver.switchTo().alert().getText();	
		 
		 
		 String popUpWithError = "Please fill out origin";
		 
		 assertEquals(popUpWithError , alertMessage);
		 
	}
	
	@Test
	public void searchOneFlyWithDestinationInvalid() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Economy";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "@#@$@#%$@!@$#!@";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-01-2023";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(5000);
		
	
		printScreen("toDestination", "invalid");
		
		Alert alert = driver.switchTo().alert();	
		
		 String alertMessage= driver.switchTo().alert().getText();	
		 
		 
		 String popUpWithError = "invalid destination";
		 
		 assertEquals(popUpWithError , alertMessage);
	}
	
	@Test
	public void searchOneFlyWithDestinationNull() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Economy";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-01-2023";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(5000);
		
	
		printScreen("toDestination", "null");
		
		Alert alert = driver.switchTo().alert();	
		
		 String alertMessage= driver.switchTo().alert().getText();	
		 
		 
		 String popUpWithError = "Please fill out destination";
		 
		 assertEquals(popUpWithError , alertMessage);
	}
	
	@Test
	public void searchOneFlyValidWithDateInvalid() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Economy";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "RML";
		search.inputToDestination(toDestination);
		
		String dateTravel = "!@-@#-@#$#";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		
		printScreen("date", "invalid");
		
		Alert alert = driver.switchTo().alert();	
		
		 String alertMessage= driver.switchTo().alert().getText();	
		 
		 
		 String popUpWithError = "Invalid Date";
		 
		 assertEquals(popUpWithError , alertMessage);
	}
	
	@Test
	public void searchOneFlyValidWithDateNull() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Economy";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "RML";
		search.inputToDestination(toDestination);
		
		String dateTravel = "";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		printScreen("date", "invalid");
		
		Alert alert = driver.switchTo().alert();	
		
		 String alertMessage= driver.switchTo().alert().getText();	
		 
		 
		 String popUpWithError = "Please fill out date";
		 
		 assertEquals(popUpWithError , alertMessage);
	}
	
	@Test
	public void searchOneFlyValidWithoutAdult() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Economy";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "RML";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-03-2023";
		search.inputDepartureDate(dateTravel);
		
		
		
		
		search.clickPassengersChilds();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		printScreen("date", "invalid");
		
		String flyingFromSigle = "DXB";
		String flyingFromSingleElement = flight.getFlightFrom();
		assertEquals(flyingFromSigle, flyingFromSingleElement);
		
		String flyingDestination = "RML";
		String flyingDestinationElement = flight.getFlightDestination();
		assertEquals(flyingDestination, flyingDestinationElement);
		printScreen("without", "adult");
	}
	
	@Test
	public void searchOneFlyValidWithoutChilds() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Economy";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "RML";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-03-2023";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersInfants();
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		printScreen("date", "invalid");
		
		String flyingFromSigle = "DXB";
		String flyingFromSingleElement = flight.getFlightFrom();
		assertEquals(flyingFromSigle, flyingFromSingleElement);
		
		String flyingDestination = "RML";
		String flyingDestinationElement = flight.getFlightDestination();
		assertEquals(flyingDestination, flyingDestinationElement);
		printScreen("without", "childs");
	}
	
	@Test
	public void searchOneFlyValidWithoutInfants() throws InterruptedException {
		
		search.clickFlights();
		search.clickOneWay();
		
		String optionInTypeFlight = "Economy";
		search.selectOptionInTypeFlight(optionInTypeFlight);
		
		String flyingFrom = "DXB";
		search.inputFlyingFrom(flyingFrom);
		
		String toDestination = "RML";
		search.inputToDestination(toDestination);
		
		String dateTravel = "11-03-2023";
		search.inputDepartureDate(dateTravel);
		
		
		
		search.clickPassengersAdult();
		search.clickPassengersChilds();
		
		
		search.clickSearchButton();
		Thread.sleep(6000);
		
		printScreen("date", "invalid");
		
		String flyingFromSigle = "DXB";
		String flyingFromSingleElement = flight.getFlightFrom();
		assertEquals(flyingFromSigle, flyingFromSingleElement);
		
		String flyingDestination = "RML";
		String flyingDestinationElement = flight.getFlightDestination();
		assertEquals(flyingDestination, flyingDestinationElement);
		printScreen("without", "infants	");
	}
}
