package base;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.google.common.io.Files;

public class BaseTest {
	public static  WebDriver driver;
	
	
	
	@BeforeAll
	public static void init() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "src/drive/chromedriver/version108/chromedriver.exe");
		driver = new ChromeDriver();
		Thread.sleep(4000);
	}
	
	@BeforeEach
	public void initHomePage() {
		driver.manage().window().maximize();
		driver.get("https://phptravels.net/");
		
	}
	

	

	public void printScreen(String testName, String result) {
		var camera = (TakesScreenshot) driver;
		File printScreen = camera.getScreenshotAs(OutputType.FILE);
		try {
			Files.move(printScreen, new File("screenshots/" + testName + "_" + result + ".png" ));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@AfterAll
	public static void finalizar() {
		driver.quit();
	}
	
}
