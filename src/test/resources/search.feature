#Author: viniciusquaresma14@hotmail.com
Feature: Search for a flight

  @Test1
  Scenario: Perform a search with the one-way option checked
    Given that I am on the flight search screen
    And check the one-way option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies

    
  @Test2
  Scenario: Perform a search with the round-trip option checked
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
    
  @Test3
  Scenario: Perform a search with type flight Economy
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
    
  @Test4
  Scenario: Perform a search with type flight Economy Premium
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy Premium" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
    
  @Test5
  Scenario: Perform a search with type flight Business
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Business" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
    
  @Test6
  Scenario: Perform a search with type flight First
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "First" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
    
  @Test7
  Scenario: Perform a search with Fly from valid
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
   
   
  @Test8
  Scenario: Perform a search with Fly from invalid
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "@#$@"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    And verify the pop-up with message error "invalid flying from"
    Then view the listing with the flights and their respective prices/companies
     
  @Test9
  Scenario: Perform a search with Fly from null
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  ""
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    And verify the pop-up with message error "Please fill out origin"
    Then view the listing with the flights and their respective prices/companies
    
    
   @Test10
  Scenario: Perform a search with Destination valid
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
    
    
   @Test11
  Scenario: Perform a search with Destination invalid
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "@#$"
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    And verify the pop-up with message error "invalid destination"
    Then view the listing with the flights and their respective prices/companies
    
  @Test12
  Scenario: Perform a search with Destination null
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  ""
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    And verify the pop-up with message error "Please fill out destination"
    Then view the listing with the flights and their respective prices/companies
    
    
   @Test13
  Scenario: Perform a search with Departure date valid
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "12-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    And verify the pop-up with message error "Please fill out destination"
    Then view the listing with the flights and their respective prices/companies
    
    
    
   @Test14
  Scenario: Perform a search with Departure date invalid
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "#$-%#-$%#@"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    And verify the pop-up with message error "invalid departure date"
    Then view the listing with the flights and their respective prices/companies
    
  @Test15
  Scenario: Perform a search with Departure date null
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: ""
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    And verify the pop-up with message error "Please fill out date"
    Then view the listing with the flights and their respective prices/companies
    
    
 	@Test16
  Scenario: Perform a search without Adult option Passengers
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "13-02-2023"
    And fill in the "0" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
  @Test17
  Scenario: Perform a search with Adult option Passengers
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "13-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
   @Test18
  Scenario: Perform a search with Adult option Passengers null
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "13-02-2023"
    And fill in the "" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
  @Test19
  Scenario: Perform a search without Childs option Passengers
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "13-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "0" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
   @Test20
  Scenario: Perform a search with Childs option Passengers
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "13-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
  @Test21
  Scenario: Perform a search with Childs option Passengers null
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "13-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
    
  @Test22
  Scenario: Perform a search without Infants option Passengers
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "13-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "0" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
  @Test23
  Scenario: Perform a search with Infants option Passengers
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "13-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "2" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
    
   @Test24
  Scenario: Perform a search with Infants option Passengers null
    Given that I am on the flight search screen
    And check the round-trip option
    And select the "Economy" type flight
    And fill in the field flying from with  "DXB"
    And fill in the field with destination with  "RML"
    And fill in the departure date with the data: "13-02-2023"
    And fill in the "2" passengers Adults
    And fill in the "2" passengers Childs
    And fill in the "" passengers Infants
    When click on the search button
    Then view the listing with the flights and their respective prices/companies
  
